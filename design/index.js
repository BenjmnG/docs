#!/usr/bin/env node
// Base on https://github.com/rysolv/markdown_ssg/

// We only need this two module. Sass module is also callded in packgage.json but will be remove for V1 (philosphy: only core feature)
const fs = require('fs').promises;
const marked = require('marked');


/*
	Look at each md file in content folder. 
	Assuming their is only two (journal, tutoriel) but can work with more
*/

async function build() {
	const files = await fs.readdir('./content/');
	for (const file of files) {

		// Take first part of file name for html name (this will be the url)
		const [path] = file.split('.');

		// Read Markdown and generate HTML
		const data = await fs.readFile(`./content/${file}`, 'utf8');
		const html = generateHtml(data, path, file);

		// Write HTML to file
		await fs.writeFile(`./export/${path}.html`, html);
	}
}


function generateHtml(data, path, file) {
	// Generte HTML

	// Parse Content, the classical way
	const parsed = marked.parse(data);

	// Parse content and return an flat array of Heading 
	const lx = marked.lexer(data)
	const headings = lx.filter(token => token.type === 'heading')

	// Then create a Table of Content
	const toc = createList(headings);

	// If "Journal" format, look for ⌛ symbol in H2 (each title a day, each symbol a work time) followed by numbers. Sum it up to make a worktime count
	const dashboard = path == 'journal' ? createDashboard(headings) : '' ;

	return `
		<!DOCTYPE html>
		<html lang="en">
		<link rel="stylesheet" type="text/css" href="./font/stylesheet.css">
		<link rel="stylesheet" type="text/css" href="./css/water.css">
		<link rel="stylesheet" type="text/css" href="./css/main.css">
		<title>${path}</title>
		<meta charset="UTF-8">
		<meta name="robots" content="noindex">
		</head>
		<body>
			<header>
				<ul>
					<li><a href="tutoriel.html" class="${path == 'tutoriel' ? 'active' : ''}">Tutoriel</a></li>
					<li><a href="journal.html" class="${path == 'journal' ? 'active' : ''}">Journal</a></li>
				</ul>
			</header>
			<main id="${path}">
				<aside class="toc">${toc}</aside>
				<div class="content">${dashboard}${parsed}</div>	
			</main>
			<footer><p>Design, docs & developpement <a href="https://benjmng.eu">benjmng.eu</a><br>${new Date().getFullYear()}</p></footer>
		</body>
		</html>
	`;
}

const createList = (headings, level = 1) => {

	// This function return a clean li element
	function createHead(item){
		return `
			<li data-level="${item.depth}">
				<a href="#${item.text.replace(/\s+/g, '-').toLowerCase()}">${item.text}</a>
			</li>
			`
	}

	let parent = '';

	// To do : logical nested list. For now it's a flat list with data-attribute & css indent
	headings.forEach( (item, i) => {      			
		let li = createHead(item)
		//if(level == item.depth){
		parent += li;
		/*} if(level > item.depth){
			level = item.depth
			let ul = document.createElement('ul')
			createList(ul, corpus, level)
		}*/
	});

	return `<h2>Sommaire</h2><ul role="list">${parent}</ul>`
}

/*
	Create a Time Spend Summary by counting 
	value next to each ⌛ symbols in title.
*/
const createDashboard = (headings) => {

	// Find ⌛ symbol in heading and agregate
	let hours = 0
	headings.map(h => {
		if(h.text.match('⌛:([0-9]+)')){
			let m = h.text.match('⌛:([0-9]+)')
			hours += parseInt(m[1])
		}
	})

	// Sum up total hours in a daywork count. 1 day = 7 hours
	let dayCount = Math.round((hours / 7) * 10) / 10

	return `<header id="dashboard"><p ><span>${hours}</span> heures furent passées sur ce projet, soit <span>${dayCount}</span>jour(s) de travail</p></header>`
}

module.exports = build();
